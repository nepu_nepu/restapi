var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var routes = require('./api/routes/Routes')
routes(app);

app.listen(port);

console.log('rest api started: ' + port);
