'use strict';
module.exports = function(app) {
  var nep = require('../controllers/nepController');

  app.route('/nep')
    .get(nep.hi)
    .post(nep.bye)
    .delete(nep.die);
};
